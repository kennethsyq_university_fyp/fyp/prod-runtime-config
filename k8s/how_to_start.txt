These instructions only apply to Azure Kubernetes Serivce. Other providers will require minor edits to some config files


1) Install cert-manager into k8s (See cert/install-cert-manager.txt)
2) Apply prod_cert.yaml and staging_cert.yaml Lets Encrypt scripts into k8s from the cert folder
3) Apply nginx-azure.yaml and nginx-tcp-service.yaml into k8s from the nginx-ingress folder
4) Test with nginx-ingress/test-web-deploy.yaml, make sure everything works properly
5) Update relevant configurations in config folder as needed
6) Apply everything from the config folder into k8s
7) Apply database.yaml to k8s if we are hosting our own database
8) Apply redis.yaml to k8s if we are hosting our own redis cache
9) Apply s3storage.yaml and webapp.yaml into k8s
10) Apply everything from workers folder into k8s
