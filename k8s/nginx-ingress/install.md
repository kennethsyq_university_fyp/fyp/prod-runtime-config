Run the following to install nginx-ingress

```bash
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
helm install ingress-nginx ingress-nginx/ingress-nginx --values values.yaml --namespace ingress-nginx --create-namespace
```

Run the following to upgrade nginx-ingress

```bash
helm upgrade ingress-nginx ingress-nginx/ingress-nginx --values values.yaml --wait

# OR
helm upgrade --reuse-values ingress-nginx ingress-nginx/ingress-nginx
```

## Note
If you are using the old method, remove ingress-nginx by running the following before reinstalling
```bash
kubectl delete -f deprecated/
```