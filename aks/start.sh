#! /bin/zsh
az aks start --name aaspk8s -g aasp-k8s
az aks update -g aasp-k8s --name aaspk8s --enable-cluster-autoscaler --min-count 1 --max-count 9
