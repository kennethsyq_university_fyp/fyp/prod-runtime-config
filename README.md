Configuration to run Automated Assessment Platform in Production Mode
---

This repository allows you to deploy AASP onto a production environment, either with Docker (through Docker-Compose) or Kubernetes

# Docker or School Server First Setup
This configuration is commonly used when you are using a single machine setup. Ensure that you have [Docker Engine](https://docs.docker.com/engine/install/) and [Docker Compose](https://docs.docker.com/compose/install/) installed on the machine before continuing.

Use the [scseserver](./scseserver) folder's configuration.

1. Configure the `redis.conf` file to add your user into the system (line 24) together with the password
2. Update the password from redis in `common.env` under REDIS_PASSWORD and `docker-compose.yml` under the test command for the cache
3. Add the TINYMCE_API (line 32) in `docker-compose.yml` with your key from [Tiny](https://tiny.cloud)
4. Update your various configuration accordingly as needed (username/passwords)
5. Uncomment the `SETUP` environment variable in `docker-compose.yml` (line 35) to enable setup
6. Run the following command to bring everything up in detached mode
```bash
docker-compose up -d
```
7. Go to `https://%HOST%/setup`, replacing `%HOST%` with your hostname for the web application to start the setup flow
8. At the end of the setup, reverse Step 5, commenting the `SETUP` environment variable
9. Restart all containers with
```bash
docker-compose up -d
```
10. Proceed to use the system

# Kubernetes First Setup (Azure Kubernetes Service)
The following configuration is designed for use with the Azure Kubernetes Service (AKS) by Microsoft Azure. Slight configuration might be needed for use with the other service providers.

Also do note that you will need to change some of the variables before deploying as the files in this repository is currently used to host a live Kubernetes cluster instance itself @ https://web.aaspk8s.itachi1706.com

We will be using the [k8s](./k8s) folder so launch a terminal in that folder context. Also ensure that [kubectl](https://kubernetes.io/docs/tasks/tools/) is installed on your machine

1. Update `cert/prod_cert.yaml` and `cert/staging_cert.yaml` and replace the `email` field with your email address
2. Follow `cert/install-cert-manager.txt` to setup cert-manager on Kubernetes
3. Run the following to setup the certificates that would be used to generate an SSL certificate for your web application through the LetsEncrypt SSL provider
```bash
kubectl apply -f cert
```
4. Update `config/config.yaml` with the necessary configuration for your web application
5. Update `config/secrets.yaml` with the necessary passwords for your web application. Do note that the values are base64 encoded and you should encode your passwords in base64 before inserting it in. You can do so by running the following
```bash
# Replace password with data to encode and copy the results to secrets.yaml
echo -n 'password' | base64
```
6. Update `imagecache/cache.yaml` with the planned code language containers being used with your relevant images. This is used to cache the images onto the nodes itself for quick use
7. Run the following commands to setup kube-fledged
```bash
cd imagecache/kube-fledged
make deploy-using-yaml
cd ..
kubectl apply -f cache.yaml
cd ..
```
8. Setup nginx ingresses with the following commands
```bash
kubectl apply -f nginx-ingress/nginx-azure.yaml
kubectl apply -f nginx-ingress/nginx-tcp-service.yaml
```
9. Update `database.yaml`, `redis.yaml`, `s3storage.yaml` and `webapp.yaml` Ingress configuration and set your own DNS domain URL, replacing the relevant URLs present inside
10. At line 76 of `webapp.yaml` change `false` to `true` to setup the web application
11. Run the following commands to setup the web application
```bash
kubectl apply -f config/
kubectl apply -f webapp.yaml
kubectl apply -f redis.yaml
kubectl apply -f s3storage.yaml
kubectl apply -f database.yaml
```
12. Wait for everything to launch with `kubectl get pods -n aasp` and make sure it is all in the `Running` status
13. Lauch your website and head on to `https://%HOST%/setup` and run the setup process
14. Reverse actions made in Line 10 to disable setup mode
15. Run the following commands to start up the workers and reboot the web application in non setup mode
```bash
kubectl apply -f workers/worker-code.yaml 
kubectl apply -f workers/worker-structured.yaml 
kubectl apply -f workers/worker-submission.yaml
kubectl apply -f webapp.yaml
```
16. Wait for everything to launch with `kubectl get pods -n aasp` and make sure it is all in the `Running` status
17. Proceed to use the system